import crbtk
from crbtk.projection import Reader
from crbtk.rebin.helicalToSingleSlice import SSR
from crbtk.rebin.geomRebinning import dectCyl2par, fan2par
import os
from tqdm import tqdm


def main():
    rootdir="../ProjectionData/00.RAW/temp/"
    subdirs='quarter_DICOM-CT-PD'

    files=os.listdir(rootdir)
    files.sort()
    print files[5:]
    for f in tqdm(files[5:]):
        tqdm.write(f)

        outname=rootdir+"/../rebinned/"+f
        r=Reader()
        proj=r.Read(rootdir+"/"+f+"/"+subdirs, verbose=True)
        proj=SSR(proj, 256, attenuation_correction=True, downsample=512)
        proj.saveAsNii(outname+"_rebinned_fan.nii.gz", True)
        proj=dectCyl2par(proj)
        proj.saveAsNii(outname+"_rebinned_fan_pardect.nii.gz", True)
        proj.dectgeom['DectShape'] = 'FLAT'
        proj=fan2par(proj)
        proj.saveAsNii(outname+"_rebinned_par.nii.gz", savemetadata=True)
        del proj

if __name__ == '__main__':
    main()

