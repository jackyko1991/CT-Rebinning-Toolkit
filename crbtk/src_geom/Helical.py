from SrcGeom import SrcGeom
import numpy as np

class Helical(SrcGeom):
    def __init__(self, sod, sdd, z_0, pitch, rot_direction=False):
        self.sod = sod          # Source iso-center distance
        self.sdd = sdd          # source detector distance
        self.z_0 = z_0          # Source initial z
        self.pitch = pitch
        self.rot_direction = rot_direction # CW if falses

        pass

    def toCartesian(self, alpha):
        pass

    def fromCartesian(self, input):
        pass


SrcGeom.register(Helical)