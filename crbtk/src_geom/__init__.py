"""
Everything are implicitly defined in radians.
Cartesian coordinate along the z-axis (parallel to rotation axis) is defined to be (0, 0, z)
"""