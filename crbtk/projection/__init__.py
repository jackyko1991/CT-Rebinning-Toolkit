from pydicom.datadict import add_dict_entry
from pydicom.tag import Tag
from os.path import abspath, basename

"""
Add DICOM dictionary
"""
f = file(abspath(__file__).replace(basename(__file__), 'DICOM-CT-PD-dict_v8.txt'))
for row in f.readlines():
    r = row.split('\t')
    add_dict_entry(Tag(*(r[0][1:-1]).split(',')), r[1].split('/')[0], r[2],r[2])

from Projection import Projection
from Reader import Reader

__all__ = ['Projection', 'Reader']