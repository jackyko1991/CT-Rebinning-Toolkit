import numpy as np
from scipy.interpolate import interp2d, griddata
from ...projection import Projection
from tqdm import tqdm
from pandas import DataFrame

def fan2par(proj):
    assert proj.dectgeom['DectShape'] == 'FLAT'
    assert proj.srcgeom['mode'] == 'STACK'
    assert proj.srcgeom['geom'] == 'FANBEAM'

    N_proj=proj.srcgeom['numOfProj']
    N_col=proj.dectgeom['Columns']
    s_col=proj.dectgeom['ColumnSpacing']

    DSD=proj.srcgeom['sdd']
    SOD=proj.srcgeom['sod']

    out=Projection()
    out._data=np.zeros(proj._data.shape)
    out.srcgeom=dict(proj.srcgeom)
    out.dectgeom=dict(proj.dectgeom)
    out._metadata=DataFrame.copy(proj._metadata)

    for i in tqdm(range(proj._data.shape[0])):
        u_prim = np.arange(N_col) * s_col - N_col * s_col / 2.
        v_prim=proj._metadata['DetectorFocalCenterAngularPosition'][proj._metadata['SliceNumber']==i]
        vmax, vmin=np.max(v_prim), np.min(v_prim)

        x, y = np.meshgrid(u_prim, v_prim)
        u = DSD * np.tan(np.arcsin(x / SOD))
        v = y + np.arctan(u/DSD)
        v[v < vmin] = vmax - vmin + v[v < vmin]
        v[v > vmax] = vmin + v[v > vmax] - vmax
        # sino=interp2d(u_prim, v_prim, proj._data[i], fill_value=0)

        out._data[i]=griddata(zip(x.flatten(), y.flatten()), proj._data[i].flatten(),
                              zip(u.flatten(), v.flatten()), fill_value=0).reshape(out._data[i].shape)

    out.dectgeom['sdd'] = 0
    out.dectgeom['sod'] = 0
    out.srcgeom['geom'] = 'PARALLEL'
    return out
