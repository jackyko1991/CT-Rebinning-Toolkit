import numpy as np
from numpy import arcsin, arccos, sin, cos, pi
from DectGeom import DectGeom

class Flat(DectGeom):
    """Flat panel detectors"""
    def __init__(self, focus_dect_dist, dectpos, w, h, dimW, dimH, tilt):
        super(Flat, self).__init__(focus_dect_dist, dectpos, w, h, dimW, dimH, tilt)

        pass

    def toCartesian(self, input):
        """Return cartesian representation of the input coordinate"""
        assert len(input) == 2
        assert input[0] >= 0 & input[0] < self._dimW
        assert input[1] >= 0 & input[1] < self._dimH
        input -= self.getDim() /2.
        input = np.array([input[0] * self._pixelSize[0], 0, input[1] * self._pixelSize[1], 1])
        return self.get2CartesianTransform().dot(input)

    def fromCartesian(self, input):
        """Inverse of toCartesian"""
        assert len(input) == 4
        e = self.getFromCartesianTransform().dot(input)
        return np.array(e[::2] / self._pixelSize + self.getDim() / 2., dtype=int)

    def buildTransformMatrix(self):
        """Get the matrix from dector pixel to cartesian coordinate"""
        alpha = self.getProjectionAngle()
        offset = self._dectpos
        # tilt rotation about x-axis
        t = np.array([[1, 0, 0, 0],
                      [0, cos(self._tilt), -sin(self._tilt), 0],
                      [0, sin(self._tilt), cos(self._tilt), 0],
                      [0, 0, 0, 1]])
        # projection rotation
        t = np.array([[cos(alpha), -sin(alpha), 0, offset[0]],
                      [sin(alpha), cos(alpha), 0, offset[1]],
                      [0, 0, 1, offset[2]],
                      [0, 0, 0, 1]]) * t
        self._matrix = t

DectGeom.register(Flat)

if __name__ == '__main__':
    f = Flat(10, [100, 180, 0], 570, 600, 1024, 32, 0)
    print f.toCartesian([4, 2])
    print f.fromCartesian(f.toCartesian([4, 2]))