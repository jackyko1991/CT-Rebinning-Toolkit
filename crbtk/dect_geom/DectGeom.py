import abc
import numpy as np

class DectGeom(object):
    __metaclass__ = abc.ABCMeta
    def __init__(self, focus_dect_dist, dectpos, w, h, dimW, dimH, tilt):
        """DectGeom(isocenter, dectpos, w, h, dimW, dimH)
        Assume focus is near the source of the X-Ray, and that the iso-center lines at a point between focus and dectpos
        Data dimension of a projection is defined by the dectector geometry
        :param double focus_dect_dist:
        :param np.ndarray dectpos:
        :param double w:
        :param double h:
        :param int dimW:
        :param int dimH:
        :param double tilt: Rotate degree about x-axis
        """
        self._focus_dect_dist = focus_dect_dist
        self._dectpos = np.array([dectpos[0], dectpos[1], dectpos[2], 1])
        self._width = w
        self._height = h
        self._dimW = int(dimW)
        self._dimH = int(dimH)
        self._dectWidth = w / float(dimW)
        self._dectHeight = h / float(dimH)
        self._tilt = tilt
        self._pixelSize = np.array(self.getPixelSize())
        self._matrix = None
        self.buildTransformMatrix()

    @abc.abstractmethod
    def toCartesian(self, input):
        """Return cartesian representation of the input coordinate"""
        return

    @abc.abstractmethod
    def fromCartesian(self, input):
        """Inverse of toCartesian"""
        return


    @abc.abstractmethod
    def buildTransformMatrix(self):
        """Build the internal variable self._transMatrix which is used to transform dector coordinate to world"""
        pass


    def get2CartesianTransform(self):
        """Get the matrix from dector pixel to cartesian coordinate"""
        assert isinstance(self._matrix, np.ndarray)
        return self._matrix

    def getFromCartesianTransform(self):
        """Inverse matrix of get2CartesianTransform"""
        assert isinstance(self._matrix, np.ndarray)
        return np.linalg.inv(self._matrix)

    def setDetectorPos(self, dectpos):
        """"""
        assert len(dectpos) >= 3
        self._dectpos = np.array([dectpos[0], dectpos[1], dectpos[2], 1])
        self.buildTransformMatrix()


    def getDim(self):
        return np.array([self._dimW, self._dimH])

    def getPixelSize(self):
        return np.array([self._dectWidth, self._dectHeight])

    def getProjectionAngle(self):
        return np.arcsin(self._dectpos[0] / np.linalg.norm(self._dectpos[:2]))

